# minimal-modules

Small project to test out different aspects of WIP C++20 modules support in:
* CMake
* Clang
* GCC

See [this thread](https://gitlab.kitware.com/cmake/cmake/-/issues/18355#note_1287181) for more information.
