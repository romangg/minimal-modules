/*
    SPDX-FileCopyrightText: 2022 Roman Gilg <subdiff@gmail.com>

    SPDX-License-Identifier: MIT
*/
import printer;

#include <iostream>
#include <QDebug>

int main(int argc, char** argv) {
    std::cout << test_print() << std::endl;
    qDebug() << "Via Qt:" << test_print();

    return EXIT_SUCCESS;
}
